package ru.kiparin.whoami.Helpers;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ru.kiparin.whoami.R;

public class CalendarHelper {

    public static String getMonthName(int monthNumber) {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, monthNumber);
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        return month_date.format(cal.getTime());
    }

    public static long calendarSettings(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        return calendar.getTimeInMillis();
    }

    public static String generateGeneration(AppCompatActivity activity, int year) {
        if (year < 1946) {
            return GetStringByEnum(activity, R.string.world_war);
        } else if (year >= 1946 && year <= 1964) {
            return GetStringByEnum(activity, R.string.baby_boom);
        } else if (year >= 1965 && year <= 1980) {
            return GetStringByEnum(activity, R.string.x_generatiom);
        } else if (year >= 1981 && year <= 1996) {
            return GetStringByEnum(activity, R.string.y_generation);
        } else if (year >= 1997 && year <= 2012) {
            return GetStringByEnum(activity, R.string.z_generation);
        } else if (year > 2012) {
            return GetStringByEnum(activity, R.string.new_generation);
        }
        return GetStringByEnum(activity, R.string.hz_generation);
    }


    private static String GetStringByEnum(AppCompatActivity activity, int intString) {
        return activity.getResources().getString(intString);
    }

}