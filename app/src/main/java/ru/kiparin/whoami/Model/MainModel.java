package ru.kiparin.whoami.Model;

import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

import ru.kiparin.whoami.DataStorag.loadingData;
import ru.kiparin.whoami.DataStorag.LocalStorag;
import ru.kiparin.whoami.MainActivity;
import ru.kiparin.whoami.R;
import ru.kiparin.whoami.Helpers.CalendarHelper;

public class MainModel {

    private TextView _generationResult;
    private CalendarView _calendarGeneration;
    private Button _generationButton;
    private MainActivity _mainActivity;
    private int _year,_month,_day = 0 ;
    private boolean _step = false;

    public  MainModel(MainActivity activity)
    {
        _mainActivity = activity;
        _generationButton = activity.findViewById(R.id.generationButton);
        _calendarGeneration = activity.findViewById(R.id.calendarGeneration);
        _generationResult = activity.findViewById(R.id.generationResult);

        setData(LocalStorag.LoadData(activity.getApplicationContext()));
        
        if(!_step)
            {
                _year=2000;
                _month = 1;
                _day = 1;
            _calendarGeneration.setDate(CalendarHelper.calendarSettings(2000,1,1),true,true);
            }
        else
            {
                _calendarGeneration.setDate(CalendarHelper.calendarSettings(_year,_month,_day),true,true);
                checkGeneration();
            }
        _calendarGeneration.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
        {
            @Override
            public void onSelectedDayChange(CalendarView view,
                                            int year,
                                            int month,
                                            int dayOfMonth)
            {
                _step = true;
                _year = year;
                _month = month;
                _day = dayOfMonth;
            }
        });

    }

    public void checkGeneration()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Вы родились " + _day + " " + CalendarHelper.getMonthName(_month) + " " + _year + " года.\n");
        String generation = CalendarHelper.generateGeneration(_mainActivity ,_year);
        stringBuilder.append(generation);
        _generationResult.setText(stringBuilder.toString());

        LocalStorag.saveData(_year , _month , _day, _step,_mainActivity.getApplicationContext());
    }

    private void setData(loadingData loadData)
    {
        _step = loadData.getStep();
        _year = loadData.getYear();
        _month = loadData.getMonth();
        _day = loadData.getDay();

        LocalStorag.saveData(_year , _month , _day, _step,_mainActivity.getApplicationContext());
    }

    private String GetStringByEnum(int intString) {
       return _mainActivity.getResources().getString(intString);
    }
}
