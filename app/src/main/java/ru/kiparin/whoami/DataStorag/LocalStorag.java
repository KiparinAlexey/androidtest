package ru.kiparin.whoami.DataStorag;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalStorag {

    public  static void saveData(int year, int month, int day , boolean step, Context context)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences("Data",Context.MODE_PRIVATE).edit();
        editor.putInt("Year", year);
        editor.putInt("Month", month);
        editor.putInt("Day", day);
        editor.putBoolean("Step", step);
        editor.apply();
    };

    public static loadingData LoadData (Context context)
    {
        loadingData data = new loadingData();
        data.setYear( context.getSharedPreferences("Data",Context.MODE_PRIVATE).getInt("Year", 0));
        data.setMonth( context.getSharedPreferences("Data",Context.MODE_PRIVATE).getInt("Month", 0));
        data.setDay( context.getSharedPreferences("Data",Context.MODE_PRIVATE).getInt("Day", 0));
        data.setStep( context.getSharedPreferences("Data",Context.MODE_PRIVATE).getBoolean("Step",false));

        return  data;
    }
}
