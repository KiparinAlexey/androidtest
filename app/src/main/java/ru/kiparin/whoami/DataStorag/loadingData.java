package ru.kiparin.whoami.DataStorag;

public class loadingData
{
    private int _year,_month,_day = 0 ;
    private  boolean _step;

    public int getDay()
    {
        return _day;
    }

    public int getMonth()
    {
        return _month;
    }

    public boolean getStep()
    {
        return _step;
    }

    public int getYear()
    {
        return _year;
    }

    public void setStep(boolean value)
    {
        _step = value;
    }

    public void setDay(int value)
    {
        _day = value;
    }

    public void setMonth(int value)
    {
        _month = value;
    }

    public void setYear(int value)
    {
        _year = value;
    }
}
