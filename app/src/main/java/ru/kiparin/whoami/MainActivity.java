package ru.kiparin.whoami;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import ru.kiparin.whoami.Model.MainModel;

public class MainActivity extends AppCompatActivity {

    private MainModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.Theme_WhoAmI);
        setContentView(R.layout.activity_main);

        model = new MainModel(this);
    }

    public void generationButton_Click(View view) {
        model.checkGeneration();
    }
}